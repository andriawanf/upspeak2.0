package com.upspeak.ppb2122

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.upspeak.ppb2122.ui.login.LoggedInUserView
import com.upspeak.ppb2122.ui.login.LoginActivity

class LandingPage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_SignIn = findViewById<Button>(R.id.button_signin)
        btn_SignIn.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}